import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String data = "Lets see the Raised button",
      data2 = "Lets see the Flat button";
  int value = 0;
  void numa() {
    setState(() {
      value++;
    });
  }

  void nums() {
    setState(() {
      value--;
    });
  }

  void dataf() {
    setState(() {
      data = "Congrats It Works";
    });
  }

  void data2f() {
    setState(() {
      data2 = "Congrats It Works Too";
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Learning about buttons",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Learning Button Widget"),
        ),
        body: Center(
          child: Column(
           crossAxisAlignment: CrossAxisAlignment.center,
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(data),
              ElevatedButton(
                child: Text("Click"),
                onPressed: () => dataf(),
                
              ),
              Divider(
                color: Colors.red,
                thickness: 5.0,
              ),
              Text(data2),
              TextButton(
                onPressed: () => data2f(),
                child: Text("Click"),
                
              ),
              Divider(
                color: Colors.red,
                thickness: 5.0,
              ),
              Text("$value"),
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                IconButton(
                  icon: Icon(Icons.add),
                  onPressed: () => numa(),
                  tooltip: "Click Me to add",
                  
                ),
                IconButton(
                  icon: Icon(Icons.remove),
                  onPressed: () => nums(),
                  tooltip: "Click me to subtract",
                ),
              ]),
            ],
          ),
        ),
      ),
    );
  }
}
